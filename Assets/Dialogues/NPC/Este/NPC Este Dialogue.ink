"¡Que sorpresa!"
"Pensaba que me quedaría solo para siempre"
    + [Cebarle un mate]
        -> cebar_un_mate
    + [Irte]
        -> irte
        
        
=== cebar_un_mate ===
" (ruido de mate) "
"Gracias, está bueno el mate"
" ... "
"¿Querés que te cuente una historia?"
    +[Quedarte]
        -> escuchar_la_historia
    + [Irte]
        -> irte

=== irte ===
"¡Nos vemos!"

-> END

=== escuchar_la_historia ===
"Esta es mi historia..."
FIN

-> END