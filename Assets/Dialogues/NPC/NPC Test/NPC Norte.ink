INCLUDE globals.ink

{ npc_hug_north == false : -> main | -> already_completed}

=== main ===

Pasó mucho tiempo...
¿Me darías un abrazo?
    + [Si, claro]
        ...
        ~ npc_hug_north = true
        -> END
    + [No, lo siento]
        -> END

=== hugged ===

Si llegaste hasta aquí es porque él confió en ti.
Deberías hablar con él nuevamente.
~ npc_north_completed = true
-> END

=== already_completed ===
Gracias por el abrazo
{ npc_north_completed == false : -> hugged }

-> END