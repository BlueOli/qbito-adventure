INCLUDE globals.ink

{ npc_hug == false : -> main | -> already_chose }

=== main ===

Me darias un abrazo?
    + [Si, claro]
        ...
        ~ npc_hug = true
        -> END
    + [No, lo siento]
        -> END

=== already_chose ===
Gracias por el abrazo
-> END