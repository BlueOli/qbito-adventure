INCLUDE globals.ink

{ npc_color == "" : -> main | -> already_chose }

=== main ===

Soy el NPC que cambia de color
¿A que color queres cambiarnos?
    + [Rojo]
        -> chosen("rojo")
    + [Azul]
        -> chosen("azul")
    + [Verde]
        -> chosen("verde")
        
=== chosen(color) ===
~ npc_color = color
Cambiamos a {color}!
-> END

=== already_chose ===
Ya cambiamos de color a {npc_color}!
-> END