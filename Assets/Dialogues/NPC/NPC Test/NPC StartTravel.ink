INCLUDE globals.ink

{ npc_startTravel == false : -> main | -> already_chose }

=== main ===

¿Estás listo para iniciar tu viaje?
    + [Si, claro]
        Buena suerte.
        ~ npc_startTravel = true
        -> END
    + [No todavia]
        Está bien, no todos somos valientes.
        -> END

=== already_chose ===
{ npc_north_completed == true : -> end_game}
Ahora podes activar el botón
-> END

=== end_game ===
Nos has salvado a todos.
Has devuelto la vida a este mundo.
Hay otros, en otros mundos. Necesitan tu ayuda.
El portal se encuentra desactivado.
Por ahora disfruta de tus logros, te llamaremos cuando se abra el paso.
Gracias.
~ game_completed = true
-> END
