INCLUDE globals.ink

{ npc_hug_south == false : -> main | -> already_completed}

=== main ===

Pasó mucho tiempo...
¿Me darías un abrazo?
    + [Si, claro]
        ...
        ~ npc_hug_south = true
        -> END
    + [No, lo siento]
        -> END

=== hugged ===

Hay otros como yo, te necesitan
Sos nuestra única salvación
~ npc_south_completed = true
-> END

=== already_completed ===
Gracias por el abrazo
{ npc_south_completed == false : -> hugged }

-> END