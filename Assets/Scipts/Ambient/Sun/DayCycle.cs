using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DayCycle : MonoBehaviour
{
    private void Update()
    {
        transform.Rotate(Vector3.right, 1.8f * Time.deltaTime);
    }    
}
