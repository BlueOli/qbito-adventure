using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPC_Bloom : MonoBehaviour
{
    private Material myMaterial;

    private void Start()
    {
        myMaterial = GetComponent<MeshRenderer>().material;
    }

    private void Update()
    {
        bool npcBloom = ((Ink.Runtime.BoolValue)DialogueManager.GetInstance().GetVariableState("npc_bloom")).value;

        if (npcBloom)
        {
            myMaterial.EnableKeyword("_EMISSION");
        }
    }
}
