using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPC_B_Color : MonoBehaviour
{
    [SerializeField] private Color defaultColor = Color.black;

    [SerializeField] private Color redColor = Color.red;
    [SerializeField] private Color blueColor = Color.blue;
    [SerializeField] private Color greenColor = Color.green;

    private Material myMaterial;

    private void Start()
    {
        myMaterial = GetComponent<MeshRenderer>().material;
    }

    private void Update()
    {
        string npcColor = ((Ink.Runtime.StringValue)DialogueManager.GetInstance().GetVariableState("npc_color")).value;

        switch (npcColor)
        {
            case "":
                myMaterial.color = defaultColor;
                break;
            case "rojo":
                myMaterial.color = redColor;
                break;
            case "azul":
                myMaterial.color = blueColor;
                break;
            case "verde":
                myMaterial.color = greenColor;
                break;
            default:
                break;
        }
    }

}
