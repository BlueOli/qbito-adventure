using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPC_RandomPos : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        int xPos = 1;
        int zPos = 1;

        do
        {
            xPos = Random.Range(-4, 4);
            zPos = Random.Range(-4, 4);

            Debug.Log(xPos + "" +  zPos);
        } while (xPos == 0 && zPos == 0);         


        transform.position = new Vector3(xPos * 2, transform.position.y, zPos * 2);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
