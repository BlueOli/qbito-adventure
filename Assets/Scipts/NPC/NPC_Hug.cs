using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPC_Hug : MonoBehaviour
{
    [Header("Camera")]
    [SerializeField] private Camera myCam;

    [Header("Characters")]
    [SerializeField] private GameObject player;
    [SerializeField] private GameObject npc;

    [Header("Characters")]
    [SerializeField] string inkHugVariable;

    private float timeToMove = 2f;

    private float timeToRotate = 2f;

    private bool hugged = false;

    public bool isHugging = false;

    public bool npcHug;

    private Vector3 baseCamPos, baseCamRot;
    private float baseCamZoom;

    public string hugDirection;


    private void Update()
    {
        npcHug = ((Ink.Runtime.BoolValue)DialogueManager.GetInstance().GetVariableState(inkHugVariable)).value;

        if(npcHug && npc.layer != 11 && !hugged && !DialogueManager.GetInstance().dialogueIsPlaying)
        {
            npc.layer = 11;
        }

        if (hugged && npc.layer != 8)
        {
            npc.layer = 8;

            switch (hugDirection)
            {
                case "North":
                    StartCoroutine(Move(player, player.transform.localPosition - (Vector3.right), false));
                    player.GetComponentInChildren<ArmMovement>().HideArm();

                    StartCoroutine(ZoomCamera(baseCamZoom, false));
                    StartCoroutine(RotateCamera(baseCamRot, false, false));
                    StartCoroutine(Move(myCam.gameObject, baseCamPos, false));
                    break;

                case "East":
                    StartCoroutine(Move(player, player.transform.localPosition - (Vector3.back), false));
                    player.GetComponentInChildren<ArmMovement>().HideArm();

                    StartCoroutine(ZoomCamera(baseCamZoom, false));
                    StartCoroutine(RotateCamera(baseCamRot, false, false));
                    StartCoroutine(Move(myCam.gameObject, baseCamPos, false));
                    break;

                case "South":
                    StartCoroutine(Move(player, player.transform.localPosition - (Vector3.left), false));
                    player.GetComponentInChildren<ArmMovement>().HideArm();

                    StartCoroutine(ZoomCamera(baseCamZoom, false));
                    StartCoroutine(RotateCamera(baseCamRot, false, false));
                    StartCoroutine(Move(myCam.gameObject, baseCamPos, false));
                    break;

                case "West":
                    StartCoroutine(Move(player, player.transform.localPosition - (Vector3.forward), false));
                    player.GetComponentInChildren<ArmMovement>().HideArm();

                    StartCoroutine(ZoomCamera(baseCamZoom, false));
                    StartCoroutine(RotateCamera(baseCamRot, false, false));
                    StartCoroutine(Move(myCam.gameObject, baseCamPos, false));
                    break;
            }

            Debug.Log("Evaluando brillo..." + npc.GetComponent<NPC_HugBloom>().maxBloom);

            if (npc.GetComponent<NPC_HugBloom>().maxBloom)
            {
                npc.GetComponent<NPC_HugBloom>().BloomAfterHug();
                Debug.Log("Brillo maximo alcanzado");
            }
            else
            {
                npc.GetComponent<NPC_HugBloom>().NoBloom();
                Debug.Log("No llegaste al brillo maximo");
            }
        }
    }

    public void triggerHug()
    {

        baseCamPos = myCam.transform.position;
        baseCamRot = myCam.transform.eulerAngles;
        baseCamZoom = myCam.orthographicSize;

        Vector3 targetMove;

        hugDirection = GetHugDirection();
        Debug.Log(hugDirection);

        switch (hugDirection)
        {
            case "North":
                npc.transform.eulerAngles = new Vector3(0, 180, 0);

                targetMove = npc.transform.position + new Vector3(-9, 4.5f, -15);
                StartCoroutine(RotateCamera(new Vector3(15, 30, 0), true, false));
                StartCoroutine(Move(myCam.gameObject, targetMove, true));
                StartCoroutine(ZoomCamera(1.5f, true));
                StartCoroutine(Move(player, player.transform.position + (Vector3.right), true));

                player.GetComponentInChildren<ArmMovement>().ShowArm(true);
                break;

            case "East":
                npc.transform.eulerAngles = new Vector3(0, 270, 0);
                targetMove = npc.transform.position + new Vector3(-15, 4.5f, 9);
                StartCoroutine(ZoomCamera(1.5f, true));
                StartCoroutine(RotateCamera(new Vector3(15, 30 + 90, 0), true, false));
                StartCoroutine(Move(myCam.gameObject, targetMove, true));

                StartCoroutine(Move(player, player.transform.position + (Vector3.back), true));

                player.GetComponentInChildren<ArmMovement>().ShowArm(true);
                break;

            case "South":
                npc.transform.eulerAngles = new Vector3(0, 0, 0);

                targetMove = npc.transform.position + new Vector3(9, 4.5f, -15);
                StartCoroutine(ZoomCamera(1.5f, true));
                StartCoroutine(RotateCamera(new Vector3(15, -30, 0), true, false));
                StartCoroutine(Move(myCam.gameObject, targetMove, true));

                StartCoroutine(Move(player, player.transform.position + (Vector3.left), true));

                player.GetComponentInChildren<ArmMovement>().ShowArm(false);
                break;

            case "West":
                npc.transform.eulerAngles = new Vector3(0, 90, 0);
                targetMove = npc.transform.position + new Vector3(-15, 4.5f, -9);
                StartCoroutine(ZoomCamera(1.5f, true));
                StartCoroutine(RotateCamera(new Vector3(15, 60, 0), true, false));
                StartCoroutine(Move(myCam.gameObject, targetMove, true));

                StartCoroutine(Move(player, player.transform.position + (Vector3.forward), true));
                player.GetComponentInChildren<ArmMovement>().ShowArm(false);
                break;
        }
    }

    private string GetHugDirection()
    {
        Vector3 posDistance = npc.transform.position - player.transform.position;

        string playerFacingDirection = "";

        if(posDistance.normalized.x > 0)
        {
            playerFacingDirection = "North";
        }
        else if(posDistance.normalized.x < 0)
        {
            playerFacingDirection = "South";
        }
        else if (posDistance.normalized.z < 0)
        {
            playerFacingDirection = "East";
        }
        else if(posDistance.normalized.z > 0)
        {
            playerFacingDirection = "West";
        }

        return playerFacingDirection;
    }

    private IEnumerator Move(GameObject obj, Vector3 target, bool isHug)
    {
        bool forcedStopMove = false;

        isHugging = true;

        float elapsedTime = 0;

        Vector3 origPos = obj.transform.position;
        Vector3 targetPos = target;

        while (elapsedTime < timeToMove)
        {
            obj.transform.position = Vector3.Lerp(origPos, targetPos, (elapsedTime / timeToMove));
            elapsedTime += Time.deltaTime;

            if (Input.GetKeyUp(KeyCode.E))
            {
                elapsedTime = timeToMove;
                forcedStopMove = true;
            }

            yield return null;
        }

        if (isHug)
        {
            if (forcedStopMove)
            {
                obj.transform.position = origPos;
                player.GetComponentInChildren<ArmMovement>().DisappearArm();
            }
            else
            {
                npc.GetComponent<NPC_HugBloom>().BloommingNPC(14, 10);

                while (!Input.GetKeyUp(KeyCode.E))
                {
                    yield return null;
                }

                obj.transform.position = targetPos;
                hugged = true;
                
            }
        }
        else
        {
            obj.transform.position = targetPos;
        }        

        isHugging = false;              
    }

    private IEnumerator ZoomCamera(float target, bool isHug)
    {
        bool forcedStopZoom = false;

        float elapsedTime = 0;

        float origZoom = myCam.orthographicSize;
        float targetZoom = target;

        while (elapsedTime < timeToMove)
        {
            myCam.orthographicSize = Vector2.Lerp(new Vector2(origZoom, 0), new Vector2(targetZoom, 0), (elapsedTime / timeToMove)).x;
            elapsedTime += Time.deltaTime;

            if (Input.GetKeyUp(KeyCode.E))
            {
                elapsedTime = timeToMove;
                forcedStopZoom = true;
            }

            yield return null;
        }


        if (isHug)
        {
            if (forcedStopZoom)
            {
                myCam.orthographicSize = origZoom;
            }
            else
            {
                while (!Input.GetKeyUp(KeyCode.E))
                {
                    yield return null;
                }

                myCam.orthographicSize = target;
            }
        }
        else
        {
            myCam.orthographicSize = target;
        }
        
        
    }

    private IEnumerator RotateCamera(Vector3 target, bool isHug, bool vueltaCompleta)
    {
        bool forcedStopRotate = false;

        float elapsedTime = 0;

        Vector3 origRot = myCam.transform.eulerAngles;
        Vector3 targetRot = target;

        Debug.Log("Sin modificar:");
        Debug.Log("Original: " + origRot + " / Target: " + targetRot);

        if (!vueltaCompleta)
        {
            if ((int)origRot.x > (int)targetRot.x && origRot.x > 180)
            {
                origRot = new Vector3(origRot.x - 360, origRot.y, origRot.z);
            }

            if ((int)origRot.y > (int)targetRot.y && origRot.y > 180)
            {
                origRot = new Vector3(origRot.x, origRot.y - 360, origRot.z);
            }

            if ((int)origRot.z > (int)targetRot.z && origRot.z > 180)
            {
                origRot = new Vector3(origRot.x, origRot.y, origRot.z - 360);
            }

            if (targetRot.x > 180)
            {
                targetRot = new Vector3(targetRot.x - 360, targetRot.y, targetRot.z);
            }

            if (targetRot.y > 180)
            {
                targetRot = new Vector3(targetRot.x, targetRot.y - 360, targetRot.z);
            }

            if (targetRot.z > 180)
            {
                targetRot = new Vector3(targetRot.x, targetRot.y, targetRot.z - 360);
            }

            Debug.Log("Despues de modificar:");
            Debug.Log("Original: " + origRot + " / Target: " + targetRot);
        }

        while (elapsedTime < timeToRotate)
        {
            myCam.transform.eulerAngles = Vector3.Lerp(origRot, targetRot, (elapsedTime / timeToRotate));
            elapsedTime += Time.deltaTime;

            if (Input.GetKeyUp(KeyCode.E))
            {
                elapsedTime = timeToRotate;
                forcedStopRotate = true;
            }

            yield return null;
        }

        if (isHug)
        {
            if (forcedStopRotate)
            {
                myCam.transform.eulerAngles = origRot;
            }
            else
            {
                while (!Input.GetKeyUp(KeyCode.E))
                {
                    yield return null;
                }

                myCam.transform.eulerAngles = targetRot;
            }
        }
        else
        {
            myCam.transform.eulerAngles = targetRot;
        } 
        
    }
}
