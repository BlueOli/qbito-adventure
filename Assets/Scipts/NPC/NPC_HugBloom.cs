using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPC_HugBloom : MonoBehaviour
{
    private Material myMat;

    Color baseColor;
    Color offColor;
    Color currentColor;

    public bool maxBloom = false;

    // Start is called before the first frame update
    void Start()
    {
        myMat = GetComponent<MeshRenderer>().material;
        baseColor = myMat.GetColor("_EmissionColor");
    }

    public void BloommingNPC(int intensity, float time)
    {
        StartCoroutine(BloomNPC(intensity, time, true));
    }

    public void BloomAfterHug()
    {
        StartCoroutine(BloomNPC(6, 4f, false));
    }

    public void NoBloom()
    {
        StartCoroutine(BloomNPC(0, 1f, false));
    }

    private IEnumerator BloomNPC(int intensity, float time, bool isHug)
    {
        float timeToColor = time; 
        float elapsedTime = 0;
        bool forcedStopMove = false;

        currentColor = myMat.GetColor("_EmissionColor");


        if (isHug)
        {
            timeToColor = time / intensity;

            for (int i = 0; i < intensity; i++)
            {
                offColor = baseColor * (Mathf.Pow(2, i));

                while (elapsedTime < timeToColor)
                {
                    myMat.SetColor("_EmissionColor", Vector4.Lerp(currentColor, offColor, (elapsedTime / timeToColor)));

                    elapsedTime += Time.deltaTime;

                    if (Input.GetKeyUp(KeyCode.E))
                    {
                        elapsedTime = timeToColor;
                        forcedStopMove = true;
                        i = intensity;
                    }

                    yield return null;
                }

                elapsedTime = 0;
                currentColor = myMat.GetColor("_EmissionColor");
            }
        }
        else
        {
            offColor = baseColor * (Mathf.Pow(2, intensity));

            while (elapsedTime < timeToColor)
            {
                myMat.SetColor("_EmissionColor", Vector4.Lerp(currentColor, offColor, (elapsedTime / timeToColor)));

                elapsedTime += Time.deltaTime;

                yield return null;
            }
        }

        

        if (isHug)
        {
            if (forcedStopMove)
            {
                NoBloom();
            }
            else
            {
                while (!Input.GetKeyUp(KeyCode.E))
                {
                    yield return null;
                }

                maxBloom = true;
                myMat.SetColor("_EmissionColor", offColor);
            }
        }
        else
        {
            myMat.SetColor("_EmissionColor", offColor);
        }
       
    }
}
