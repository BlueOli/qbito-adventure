using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarreraBorde : MonoBehaviour
{
    [SerializeField] Rigidbody rig;
    [SerializeField] Transform tr;
    [SerializeField] Collider col;

    [Space(22)] [SerializeField] Vector3 closestPointOnCollider;

    public GameObject barrier;


    // Start is called before the first frame update
    void Start()
    {
        closestPointOnCollider = Physics.ClosestPoint(point: new Vector3(1, 1), collider: col, position: tr.position, rotation: tr.rotation);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            closestPointOnCollider = Physics.ClosestPoint(point: new Vector3(1, 0, 1), collider: col, position: tr.position, rotation: tr.rotation);
            closestPointOnCollider.y += 0.5f;

            Instantiate(barrier, closestPointOnCollider, Quaternion.identity);

            closestPointOnCollider = Physics.ClosestPoint(point: new Vector3(1, 0, -1), collider: col, position: tr.position, rotation: tr.rotation);
            closestPointOnCollider.y += 0.5f;

            Instantiate(barrier, closestPointOnCollider, Quaternion.identity);

            closestPointOnCollider = Physics.ClosestPoint(point: new Vector3(-1, 0, 1), collider: col, position: tr.position, rotation: tr.rotation);
            closestPointOnCollider.y += 0.5f;

            Instantiate(barrier, closestPointOnCollider, Quaternion.identity);

            closestPointOnCollider = Physics.ClosestPoint(point: new Vector3(-1, 0, -1), collider: col, position: tr.position, rotation: tr.rotation);
            closestPointOnCollider.y += 0.5f;

            Instantiate(barrier, closestPointOnCollider, Quaternion.identity);



        }
    }
}
