using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NPC_TalkTrigger : MonoBehaviour
{
    [Header("Ink Variable")]
    [SerializeField] private string inkVar;
    [SerializeField] private bool isButtonTrigger;

    [Header("Scene Change")]
    [SerializeField] private bool isSceneChange;
    [SerializeField] private string sceneName;


    bool npcTalk;

    // Update is called once per frame
    void Update()
    {
        npcTalk = ((Ink.Runtime.BoolValue)DialogueManager.GetInstance().GetVariableState(inkVar)).value;


        if (isSceneChange)
        {
            if (npcTalk && !DialogueManager.GetInstance().dialogueIsPlaying)
            {
                SceneManager.LoadScene(sceneName);
            }
        }

        if(isButtonTrigger) 
        {
            if (npcTalk && this.gameObject.layer != 10)
            {
                this.gameObject.layer = 10;
            }
        }        
    }
}
