using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonBloom : MonoBehaviour
{
    private Material myMat;

    private void Start()
    {
        myMat = GetComponent<MeshRenderer>().material;
    }

    // Update is called once per frame
    void Update()
    {
        if(gameObject.layer == 10 && !myMat.IsKeywordEnabled("_EMISSION"))
        {
            myMat.EnableKeyword("_EMISSION");
        }
        else if (gameObject.layer != 10 && myMat.IsKeywordEnabled("_EMISSION"))
        {
            myMat.DisableKeyword("_EMISSION");
        }
    }
}
