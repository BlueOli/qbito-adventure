using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonLift : MonoBehaviour
{
    public List<GameObject> targets;
    public bool platformsMoving = false;

    public void Activate()
    {
        foreach (GameObject obj in targets)
        {
            if (obj.transform.localPosition == obj.GetComponent<LiftPlatform>().endPos)
            {
                obj.GetComponent<LiftPlatform>().Disappear();
            }
            else
            {
                obj.GetComponent<LiftPlatform>().Appear();
            }      
        }
    }

    public bool TargetsMoving()
    {
        platformsMoving = false;

        foreach (GameObject obj in targets)
        {
            if (obj.GetComponent<LiftPlatform>().isMoving)
            {
                platformsMoving = true;
            }
        }

        return platformsMoving;
    }
}
