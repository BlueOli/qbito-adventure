using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonConnect : MonoBehaviour
{
    public GameObject button;

    public void Connect()
    {
        if (!button.activeSelf)
        {
            button.SetActive(true);
            this.transform.gameObject.SetActive(false);
        }
    }
}
