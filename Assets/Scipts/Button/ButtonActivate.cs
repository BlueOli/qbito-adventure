using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonActivate : MonoBehaviour
{
    public List<GameObject> targets;

    public void Activate()
    {
        foreach(GameObject obj in targets)
        {
            if (obj.activeSelf)
            {
                obj.SetActive(true);
            }
            else
            {
                obj.SetActive(true);
            }
        }
    }
}
