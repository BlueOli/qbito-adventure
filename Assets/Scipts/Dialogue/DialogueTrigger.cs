using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueTrigger : MonoBehaviour
{
    public TextAsset inkJSON;

    public void Dialogue()
    {
        DialogueManager.GetInstance().EnterDialogueMode(inkJSON);
    }
}
