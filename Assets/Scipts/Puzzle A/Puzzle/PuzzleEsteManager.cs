using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleEsteManager : MonoBehaviour
{
    // Start is called before the first frame update

    public List<GameObject> bridgeTriggers;

    public List<GameObject> bridgeConstraints;

    public List<GameObject> buttons;

    public List<GameObject> nextLevelButton;

    // Update is called once per frame
    void Update()
    {
        bool isCompleted = true;

        foreach(GameObject obj in bridgeTriggers)
        {
            if(!(obj.transform.localPosition == obj.GetComponent<LiftPlatform>().endPos))
            {
                isCompleted = false;
            }
        }

        foreach (GameObject obj in bridgeConstraints)
        {
            if (obj.transform.localPosition == obj.GetComponent<LiftPlatform>().endPos)
            {
                isCompleted = false;
            }
        }

        if (isCompleted)
        {
            foreach(GameObject obj in buttons)
            {
                obj.layer = 0;
            }

            foreach(GameObject obj in nextLevelButton)
            {
                obj.layer = 10;
            }

            transform.GetComponent<PuzzleEsteManager>().enabled = false;
        }
    }
}
