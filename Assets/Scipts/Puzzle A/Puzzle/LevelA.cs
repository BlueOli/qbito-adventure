using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelA : MonoBehaviour
{
    // Start is called before the first frame update

    public List<GameObject> triggers;

    public List<GameObject> buttons;

    public List<GameObject> mainBridge;

    public List<GameObject> residual;

    // Update is called once per frame
    void Update()
    {
        bool isCompleted = true;

        foreach(GameObject obj in triggers)
        {
            if(!(obj.transform.localPosition == obj.GetComponent<LiftPlatform>().endPos))
            {
                isCompleted = false;
            }
        }

        if (isCompleted)
        {
            foreach(GameObject obj in buttons)
            {
                obj.layer = 9;
            }

            foreach(GameObject obj in mainBridge)
            {
                obj.GetComponent<LiftPlatform>().Appear();

            }

            foreach (GameObject obj in residual)
            {
                obj.GetComponent<LiftPlatform>().Disappear();

            }

            transform.GetComponent<LevelA>().enabled = false;
        }
    }
}
