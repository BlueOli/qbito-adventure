using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LiftPlatform : MonoBehaviour
{
    public Vector3 startPos, endPos;
    private Vector3 origPos, targetPos;
    private float timeToMove = 1f;

    public bool isMoving;

    public bool isFloor = false;

    private void Start()
    {
        if(gameObject.layer == 3)
        {
            isFloor = true;
        }

        for(int i = 0; i < transform.childCount; i++)
        {
            if(transform.GetChild(i).gameObject.layer == 3)
            {
                isFloor = true;
            }
        }        
    }

    private IEnumerator MovePlatform(Vector3 target)
    {
        isMoving = true;

        if (isFloor) TemporalFloorToogle();


        float elapsedTime = 0;

        origPos = transform.localPosition;
        targetPos = target;

        while (elapsedTime < timeToMove)
        {
            transform.localPosition = Vector3.Lerp(origPos, targetPos, (elapsedTime / timeToMove));
            elapsedTime += Time.deltaTime;
            yield return null;
        }

        transform.localPosition = targetPos;

        isMoving = false;

        if (isFloor) TemporalFloorToogle();
    }

    public void Appear()
    {
        if (!isMoving)
        {
            StartCoroutine(MovePlatform(endPos));
        }
    }

    public void Disappear()
    {
        if (!isMoving)
        {
            StartCoroutine(MovePlatform(startPos));
        }
    }

    private void TemporalFloorToogle()
    {
        if (gameObject.layer == 3)
        {
            gameObject.layer = 30;
        }
        else if (gameObject.layer == 30)
        {
            gameObject.layer = 3;
        }

        for (int i = 0; i < transform.childCount; i++)
        {
            if (transform.GetChild(i).gameObject.layer == 3)
            {
                transform.GetChild(i).gameObject.layer = 30;
            }
            else if(transform.GetChild(i).gameObject.layer == 30)
            {
                transform.GetChild(i).gameObject.layer = 3;
            }
        }
    }
}
