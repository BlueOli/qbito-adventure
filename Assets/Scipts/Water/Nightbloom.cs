using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Nightbloom : MonoBehaviour
{
    public GameObject sun;

    private Material waterMat;

    Vector4 baseColor;
    Vector4 offColor;
    Vector4 currentColor;

    private bool isColoring;

    private float timeToColor = 44f;
    // Start is called before the first frame update

    private bool medioCiclo;
    private bool dejarPasarTiempo;

    void Start()
    {
        waterMat = GetComponent<MeshRenderer>().material;
        baseColor = waterMat.GetColor("_CausticColor");

        medioCiclo = true;
    }

    void Update()
    {
        if (sun.transform.eulerAngles.x > 0
            && sun.transform.eulerAngles.x < 1)
        {
            if (!isColoring && medioCiclo)
            {
                medioCiclo = false;
                Debug.Log("Parte A");
                isColoring = true;
                StartCoroutine(BloomCaustic(4f));
                dejarPasarTiempo = true;
            }
            else if(!isColoring && !medioCiclo)
            {
                if (dejarPasarTiempo)
                {
                    dejarPasarTiempo = false;
                    StartCoroutine(DejarPasarElTiempo(5));
                } 
            }
        }

        if (sun.transform.eulerAngles.x > 270
            && sun.transform.eulerAngles.x < 271)
        {
            if (!isColoring)
            {
                Debug.Log("Parte B");
                isColoring = true;
                StartCoroutine(BloomCaustic(0.25f));
            }
        }
    }

    private IEnumerator BloomCaustic(float intensity)
    {
        float elapsedTime = 0;

        currentColor = waterMat.GetColor("_CausticColor");
        offColor = waterMat.GetColor("_CausticColor") * intensity;

        Debug.Log("Bloom start");

        while (elapsedTime < timeToColor)
        {
            waterMat.SetVector("_CausticColor", Vector4.Lerp(currentColor, offColor, (elapsedTime/timeToColor)));

            elapsedTime += Time.deltaTime;

            yield return null;
        }

        waterMat.SetVector("_CausticColor", offColor);

        isColoring = false;

        Debug.Log("Bloom end");
    }

    private IEnumerator DejarPasarElTiempo(int segundos)
    {
        yield return new WaitForSeconds(segundos);
        medioCiclo = true;
    }

}