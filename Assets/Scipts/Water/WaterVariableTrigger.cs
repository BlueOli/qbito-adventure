using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterVariableTrigger : MonoBehaviour
{
    [Header("Player")]
    [SerializeField] private GameObject player;

    [Header("Water")]
    [SerializeField] private GameObject water;

    [Header("Camera")]
    [SerializeField] private GameObject gameObjCam;
    [SerializeField] private float timeToMove = 2.5f;

    [Header("Variable")]
    [SerializeField] private string nameVar;
    [SerializeField] private float intensity;
    [SerializeField] private float timeToChange = 2.5f;
    [SerializeField] private bool isColor = false;
    [SerializeField] private string choiceColor;

    private Material waterMat;

    float baseVar;
    float offVar;
    float currentVar;

    Vector4 baseColor;
    Vector4 offColor;
    Vector4 currentColor;

    private bool isChanging;

    private float origZoom, targetZoom;

    private Camera cam;

    void Start()
    {
        waterMat = water.transform.GetComponent<MeshRenderer>().material;
        cam = gameObjCam.transform.GetComponent<Camera>();
        baseVar = waterMat.GetFloat(nameVar);
    }

    private IEnumerator ShowCaustic(float intensity)
    {
        isChanging = true;

        float elapsedTime = 0;

        currentVar = waterMat.GetFloat(nameVar);
        offVar = waterMat.GetFloat(nameVar) + intensity;

        while (elapsedTime < timeToChange)
        {
            waterMat.SetFloat(nameVar, Mathf.Lerp(currentVar, offVar, (elapsedTime / timeToChange)));

            elapsedTime += Time.deltaTime;

            yield return null;
        }

        waterMat.SetFloat(nameVar, offVar);

        isChanging = false;

        this.gameObject.SetActive(false);
    }

    private IEnumerator ShowColor(float intensity)
    {
        isChanging = true;

        float elapsedTime = 0;

        currentColor = waterMat.GetColor(nameVar);

        switch (choiceColor)
        {
            case "Red":
                offColor = waterMat.GetColor(nameVar) + new Color(intensity, 0f, 0f, 0f);
                break;
            case "Green":
                offColor = waterMat.GetColor(nameVar) + new Color(0f, intensity, 0f, 0f);
                break;
            case "Blue":
                offColor = waterMat.GetColor(nameVar) + new Color(0f, 0f, intensity, 0f);
                break;
            default:   
                break;  
        }
        

        while (elapsedTime < timeToChange)
        {
            waterMat.SetColor(nameVar, Vector4.Lerp(currentColor, offColor, (elapsedTime / timeToChange)));

            elapsedTime += Time.deltaTime;

            yield return null;
        }

        waterMat.SetFloat(nameVar, offVar);

        isChanging = false;

        this.gameObject.SetActive(false);
    }

    private IEnumerator ZoomCamera(float target)
    {
        float elapsedTime = 0;

        origZoom = cam.orthographicSize;
        targetZoom = cam.orthographicSize + target;

        while (elapsedTime < timeToMove)
        {
            cam.orthographicSize = Vector2.Lerp(new Vector2(origZoom, 0), new Vector2(targetZoom, 0), (elapsedTime / timeToMove)).x;
            elapsedTime += Time.deltaTime;
            yield return null;
        }

        cam.orthographicSize = targetZoom;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == player)
        {
            if (!isChanging)
            {
                Debug.Log("Hit, changing " + nameVar);
                if (!isColor)
                {
                    StartCoroutine(ShowCaustic(intensity));
                }
                else
                {
                    StartCoroutine(ShowColor(intensity));
                }
                StartCoroutine(ZoomCamera(0.5f));
            }
        }
    }
}
