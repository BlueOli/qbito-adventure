using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CausticShow : MonoBehaviour
{
    [Header("Player")]
    [SerializeField] private GameObject player;

    [Header("Water")]
    [SerializeField] private GameObject water;

    [Header("Camera")]
    [SerializeField] private GameObject gameObjCam;

    private Material waterMat;

    float baseCaustic;
    float offCaustic;
    float currentCaustic;

    private bool isChanging;

    private float timeToChange = 2.5f;
    // Start is called before the first frame update

    private float timeToMove = 2.5f;

    private float origZoom, targetZoom;

   
    private Camera cam;

    void Start()
    {
        waterMat = water.transform.GetComponent<MeshRenderer>().material;
        cam = gameObjCam.transform.GetComponent<Camera>();
        baseCaustic = waterMat.GetFloat("_CausticCutout");
    }

    private IEnumerator ShowCaustic(float intensity)
    {
        isChanging = true;

        float elapsedTime = 0;

        currentCaustic = waterMat.GetFloat("_CausticCutout");
        offCaustic = waterMat.GetFloat("_CausticCutout") + intensity;

        while (elapsedTime < timeToChange)
        {
            waterMat.SetFloat("_CausticCutout", Mathf.Lerp(currentCaustic, offCaustic, (elapsedTime / timeToChange)));

            elapsedTime += Time.deltaTime;

            yield return null;
        }

        waterMat.SetFloat("_CausticCutout", offCaustic);

        isChanging = false;

        this.gameObject.SetActive(false);
    }

    private IEnumerator ZoomCamera(float target)
    {
        float elapsedTime = 0;

        origZoom = cam.orthographicSize;
        targetZoom = cam.orthographicSize + target;

        while (elapsedTime < timeToMove)
        {
            cam.orthographicSize = Vector2.Lerp(new Vector2(origZoom, 0), new Vector2(targetZoom, 0), (elapsedTime / timeToMove)).x;
            elapsedTime += Time.deltaTime;
            yield return null;
        }

        cam.orthographicSize = targetZoom;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject == player)
        {
            if (!isChanging)
            {
                Debug.Log("Hit, change caustic");
                StartCoroutine(ShowCaustic(-0.1f));
                StartCoroutine(ZoomCamera(0.5f));
            }            
        }
    }
}
