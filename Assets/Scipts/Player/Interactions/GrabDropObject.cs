using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabDropObject : MonoBehaviour
{
    public GameObject grabObject;
    public bool isGrabbingObject = false;
    
    public void Grab(GameObject grab)
    {
        this.grabObject = grab;
        grabObject.transform.parent = this.transform;
        grabObject.transform.localPosition = new Vector3(0f, 0.925f, 0f);
        isGrabbingObject = true;
    }

    public void Drop()
    {
        grabObject.transform.localPosition = new Vector3(-1f, 0.175f, 0f);
        grabObject.transform.parent = null;
        isGrabbingObject = false;
    }

    public void Connect(GameObject pole)
    {
        grabObject.transform.parent = pole.transform;
        grabObject.transform.localPosition = new Vector3(0f, 0.5f, 0f);
        grabObject = null;
        isGrabbingObject = false;
        pole.GetComponent<ButtonConnect>().Connect();
    }

}
