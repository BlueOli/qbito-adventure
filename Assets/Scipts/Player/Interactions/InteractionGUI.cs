using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractionGUI : MonoBehaviour
{
    [Header("Camera")]
    [SerializeField] private GameObject gameObjCamera;


    void Update()
    {
        if(Physics.Raycast(transform.position + Vector3.up*0.5f, GetFaceDirection(), out RaycastHit hitinfo, 2f))
        {
            if(hitinfo.transform.gameObject.layer == 6)
            {
                transform.GetChild(2).gameObject.SetActive(true);
                transform.GetChild(2).GetChild(0).gameObject.transform.eulerAngles = 
                    new Vector3(transform.GetChild(2).GetChild(0).gameObject.transform.eulerAngles.x, 
                                gameObjCamera.transform.eulerAngles.y + 90,
                                transform.GetChild(2).GetChild(0).gameObject.transform.eulerAngles.z);
                if (Input.GetKeyDown(KeyCode.E))
                {
                    hitinfo.transform.GetComponent<ButtonActivate>().Activate();
                }
            }
            else
            {
                transform.GetChild(2).gameObject.SetActive(false);
            }

            if (hitinfo.transform.gameObject.layer == 10
                && !hitinfo.transform.GetComponent<ButtonLift>().TargetsMoving())
            {
                transform.GetChild(2).gameObject.SetActive(true);
                transform.GetChild(2).GetChild(0).gameObject.transform.eulerAngles =
                    new Vector3(transform.GetChild(2).GetChild(0).gameObject.transform.eulerAngles.x,
                                gameObjCamera.transform.eulerAngles.y + 90,
                                transform.GetChild(2).GetChild(0).gameObject.transform.eulerAngles.z);
                if (Input.GetKeyDown(KeyCode.E))
                {
                    hitinfo.transform.GetComponent<ButtonLift>().Activate();
                }
            }
            else
            {
                transform.GetChild(2).gameObject.SetActive(false);
            }

            if (hitinfo.transform.gameObject.layer == 8)
            {
                if (!DialogueManager.GetInstance().dialogueIsPlaying &&
                    !hitinfo.transform.GetComponent<NPC_Hug>().isHugging)
                {
                    transform.GetChild(3).gameObject.SetActive(true);
                    transform.GetChild(3).GetChild(0).gameObject.transform.eulerAngles =
                    new Vector3(transform.GetChild(3).GetChild(0).gameObject.transform.eulerAngles.x,
                                gameObjCamera.transform.eulerAngles.y + 90,
                                transform.GetChild(3).GetChild(0).gameObject.transform.eulerAngles.z);
                    if (Input.GetKeyDown(KeyCode.E))
                    {
                        RotateNPC(hitinfo.transform.gameObject, GetFaceDirection());
                        hitinfo.transform.GetComponent<DialogueTrigger>().Dialogue();
                    }
                }
                else
                {
                    transform.GetChild(3).gameObject.SetActive(false);
                }
            }
            else
            {
                transform.GetChild(3).gameObject.SetActive(false);
            }

            if (hitinfo.transform.gameObject.layer == 11)
            {
                if (!hitinfo.transform.GetComponent<NPC_Hug>().isHugging)
                {
                    transform.GetChild(4).gameObject.SetActive(true);
                    transform.GetChild(4).GetChild(0).gameObject.transform.eulerAngles =
                    new Vector3(transform.GetChild(4).GetChild(0).gameObject.transform.eulerAngles.x,
                                gameObjCamera.transform.eulerAngles.y + 90,
                                transform.GetChild(4).GetChild(0).gameObject.transform.eulerAngles.z);
                    if (Input.GetKeyDown(KeyCode.E))
                    {
                        RotateNPC(hitinfo.transform.gameObject, GetFaceDirection());
                        hitinfo.transform.GetComponent<NPC_Hug>().triggerHug();
                    }
                }
                else
                {
                    transform.GetChild(4).gameObject.SetActive(false);
                }
            }
            else
            {
                transform.GetChild(4).gameObject.SetActive(false);
            }

            if (hitinfo.transform.gameObject.layer == 12)
            {
                transform.GetChild(5).gameObject.SetActive(true);
                transform.GetChild(5).GetChild(0).gameObject.transform.eulerAngles =
                    new Vector3(transform.GetChild(5).GetChild(0).gameObject.transform.eulerAngles.x,
                                gameObjCamera.transform.eulerAngles.y + 90,
                                transform.GetChild(5).GetChild(0).gameObject.transform.eulerAngles.z);
                if (Input.GetKeyDown(KeyCode.E))
                {
                    this.GetComponent<GrabDropObject>().Grab(hitinfo.transform.gameObject);
                }
            }
            

            if (hitinfo.transform.gameObject.layer == 13 && this.GetComponent<GrabDropObject>().isGrabbingObject)
            {
                transform.GetChild(5).gameObject.SetActive(true);
                transform.GetChild(5).GetChild(0).gameObject.transform.eulerAngles =
                    new Vector3(transform.GetChild(5).GetChild(0).gameObject.transform.eulerAngles.x,
                                gameObjCamera.transform.eulerAngles.y + 90,
                                transform.GetChild(5).GetChild(0).gameObject.transform.eulerAngles.z);
                if (Input.GetKeyDown(KeyCode.E))
                {
                    this.GetComponent<GrabDropObject>().Connect(hitinfo.transform.gameObject);
                    transform.GetChild(5).gameObject.SetActive(false);
                }
            }

        }
        else
        {
            transform.GetChild(2).gameObject.SetActive(false);
            transform.GetChild(3).gameObject.SetActive(false);
            transform.GetChild(4).gameObject.SetActive(false);
            transform.GetChild(5).gameObject.SetActive(false);
        }
    }

    private Vector3 GetFaceDirection()
    {
        switch (transform.GetChild(0).eulerAngles.y)
        {
            case 0:
                return Vector3.right;
            case 90:
                return Vector3.back;
            case 180:
                return Vector3.left;
            case 270:
                return Vector3.forward;
            default:
                return Vector3.zero;
        }
    }

    private void RotateNPC(GameObject npc, Vector3 faceDirection)
    {
        if(faceDirection.x > 0)
        {
            npc.transform.eulerAngles = new Vector3(0, 180, 0);
        }
        else if(faceDirection.x < 0)
        {
            npc.transform.eulerAngles = new Vector3(0, 0, 0);
        }
        else if(faceDirection.z > 0)
        {
            npc.transform.eulerAngles = new Vector3(0, 90, 0);
        }
        else if (faceDirection.z < 0)
        {
            npc.transform.eulerAngles = new Vector3(0, 270, 0);
        }
    }
}
