using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmMovement : MonoBehaviour
{
    private Vector3 origPos, targetPos;
    private float timeToMove = 1f;

    public void ShowArm(bool onRight)
    {
        if (onRight)
        {
            StartCoroutine(MoveArm(new Vector3(0.5f, 0f, -0.5f)));
        }
        else
        {
            StartCoroutine(MoveArm(new Vector3(0.5f, 0f, 0.5f)));
        }        
    }

    public void HideArm()
    {
        StartCoroutine(MoveArm(Vector3.zero));
    }

    public void DisappearArm()
    {
        transform.localPosition = Vector3.zero;
    }


    private IEnumerator MoveArm(Vector3 target)
    {
        bool forcedStopMove = false;

        float elapsedTime = 0;

        origPos = transform.localPosition;
        targetPos = target;

        while (elapsedTime < timeToMove)
        {
            transform.localPosition = Vector3.Lerp(origPos, targetPos, (elapsedTime / timeToMove));
            elapsedTime += Time.deltaTime;

            if (Input.GetKeyUp(KeyCode.E))
            {
                elapsedTime = timeToMove;
                forcedStopMove = true;
            }

            yield return null;
        }

        if (forcedStopMove)
        {
            transform.localPosition = origPos;
        }
        else
        {
            while (!Input.GetKeyUp(KeyCode.E))
            {
                yield return null;
            }

            transform.localPosition = targetPos;
        }

    }
}
