using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotadorQbito : MonoBehaviour
{
    public GameObject jugador;

    // Update is called once per frame
    void Update()
    {
        Rotar();
    }

    private void Rotar()
    {
        if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow))
        {
            jugador.transform.GetChild(0).eulerAngles = new Vector3(0f, 0f, 0f);
        }

        if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
        {
            jugador.transform.GetChild(0).eulerAngles = new Vector3(0f, -90f, 0f);
        }

        if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))
        {
            jugador.transform.GetChild(0).eulerAngles = new Vector3(0f, -180f, 0f);
        }

        if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
        {
            jugador.transform.GetChild(0).eulerAngles = new Vector3(0f, 90f, 0f);
        }
    }
}
