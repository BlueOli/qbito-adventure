using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileMovement : MonoBehaviour
{
    private bool isMoving;
    private Vector3 origPos, targetPos;
    private float timeToMove = 0.2f;
    public KeyCode[] teclas = new KeyCode[4];

    private void Start()
    {
        teclas[0] = KeyCode.W;
        teclas[1] = KeyCode.A;
        teclas[2] = KeyCode.S;
        teclas[3] = KeyCode.D;
    }

    // Update is called once per frame
    void Update()
    {
        if (DialogueManager.GetInstance().dialogueIsPlaying)
        {
            return;
        }

        if(transform.parent != null)
        {
            if (transform.GetComponentInParent<LiftPlatform>().isMoving)
            {
                return;
            }
        }        

        Acelerar();
        Mover();
    }

    private void CorregirPos()
    {
        this.transform.position = new Vector3(Mathf.Round(this.transform.position.x),
                                              Mathf.Round(this.transform.position.y),
                                              Mathf.Round(this.transform.position.z));
    }

    private void Acelerar()
    {
        if (Input.GetKey(KeyCode.LeftShift))
        {
            timeToMove = 0.1f;
        }
        else
        {
            timeToMove = 0.2f;
        }
    }

    private void Mover()
    {
        if (Input.GetKey(teclas[0]) && !isMoving)
        {
            StartCoroutine(MovePlayer(Vector3.right*2));
        }
        if (Input.GetKey(teclas[1]) && !isMoving)
        {
            StartCoroutine(MovePlayer(Vector3.forward*2));
        }
        if (Input.GetKey(teclas[2]) && !isMoving)
        {
            StartCoroutine(MovePlayer(Vector3.left*2));
        }
        if (Input.GetKey(teclas[3]) && !isMoving)
        {
            StartCoroutine(MovePlayer(Vector3.back*2));
        }
    }

    private IEnumerator MovePlayer(Vector3 direction)
    {
        Rotar(direction);
        isMoving = true;

        float elapsedTime = 0;

        origPos = transform.position;
        targetPos = origPos + direction;

        if (ChequearCasillero(targetPos))
        {
            while (elapsedTime < timeToMove)
            {
                transform.position = Vector3.Lerp(origPos, targetPos, (elapsedTime / timeToMove));
                elapsedTime += Time.deltaTime;
                yield return null;
            }

            transform.position = targetPos;
        }

        CorregirPos();

        isMoving = false;
    }

    private void Rotar(Vector3 direction)
    {
        if (direction.normalized == Vector3.right)
        {
            transform.GetChild(0).eulerAngles = new Vector3(0f, 0f, 0f);
        }

        if (direction.normalized == Vector3.forward)
        {
            transform.GetChild(0).eulerAngles = new Vector3(0f, -90f, 0f);
        }

        if (direction.normalized == Vector3.left)
        {
            transform.GetChild(0).eulerAngles = new Vector3(0f, -180f, 0f);
        }

        if (direction.normalized == Vector3.back)
        {
            transform.GetChild(0).eulerAngles = new Vector3(0f, 90f, 0f);
        }
    }

    private bool ChequearCasillero(Vector3 targetPos)
    {
        if (Physics.Raycast(targetPos + Vector3.up * 2f, Vector3.down, out RaycastHit hitinfo, 20f))
        {
            if (hitinfo.transform.gameObject.layer != 3)
            {
                return false;
            }
        }

        return true;
    }

    public void RotarTeclas(bool clockwise)
    {
        int index = 0;
        int posW = 0;

        foreach(KeyCode key in teclas)
        {
            if(key != KeyCode.W)
            {
                index++;
            }
            else
            {
                posW = index;
            }
        }

        switch (posW)
        {
            case 0:
                if (clockwise)
                {
                    teclas[1] = KeyCode.W;
                    teclas[2] = KeyCode.A;
                    teclas[3] = KeyCode.S;
                    teclas[0] = KeyCode.D;
                }
                else
                {
                    teclas[3] = KeyCode.W;
                    teclas[0] = KeyCode.A;
                    teclas[1] = KeyCode.S;
                    teclas[2] = KeyCode.D;
                }
                break;
            case 1:
                if (clockwise)
                {
                    teclas[2] = KeyCode.W;
                    teclas[3] = KeyCode.A;
                    teclas[0] = KeyCode.S;
                    teclas[1] = KeyCode.D;
                }
                else
                {
                    teclas[0] = KeyCode.W;
                    teclas[1] = KeyCode.A;
                    teclas[2] = KeyCode.S;
                    teclas[3] = KeyCode.D;
                }
                break;
            case 2:
                if (clockwise)
                {
                    teclas[3] = KeyCode.W;
                    teclas[0] = KeyCode.A;
                    teclas[1] = KeyCode.S;
                    teclas[2] = KeyCode.D;
                }
                else
                {
                    teclas[1] = KeyCode.W;
                    teclas[2] = KeyCode.A;
                    teclas[3] = KeyCode.S;
                    teclas[0] = KeyCode.D;
                }
                break;
            case 3:
                if (clockwise)
                {
                    teclas[0] = KeyCode.W;
                    teclas[1] = KeyCode.A;
                    teclas[2] = KeyCode.S;
                    teclas[3] = KeyCode.D;
                }
                else
                {
                    teclas[2] = KeyCode.W;
                    teclas[3] = KeyCode.A;
                    teclas[0] = KeyCode.S;
                    teclas[1] = KeyCode.D;
                }
                break;
            default:
                break;
        }
    }

    private Vector3 GetFaceDirection(GameObject player)
    {
        switch (player.transform.GetChild(0).eulerAngles.y)
        {
            case 0:
                return Vector3.right;
            case 90:
                return Vector3.back;
            case 180:
                return Vector3.left;
            case 270:
                return Vector3.forward;
            default:
                return Vector3.zero;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        GameObject obj = other.gameObject;

        switch (obj.name)
        {
            case "IslaCentral":
                teclas[0] = KeyCode.W;
                teclas[1] = KeyCode.A;
                teclas[2] = KeyCode.S;
                teclas[3] = KeyCode.D;
                break;

            case "RotarNorte":
                teclas[0] = KeyCode.D;
                teclas[1] = KeyCode.W;
                teclas[2] = KeyCode.A;
                teclas[3] = KeyCode.S;
                break;

            case "RotarEste":
                teclas[0] = KeyCode.W;
                teclas[1] = KeyCode.A;
                teclas[2] = KeyCode.S;
                teclas[3] = KeyCode.D;
                break;

            case "TutorialC":
                teclas[0] = KeyCode.W;
                teclas[1] = KeyCode.A;
                teclas[2] = KeyCode.S;
                teclas[3] = KeyCode.D;
                break;

            case "RotarSur":
                teclas[0] = KeyCode.A;
                teclas[1] = KeyCode.S;
                teclas[2] = KeyCode.D;
                teclas[3] = KeyCode.W;
                break;
        }
    }

}
