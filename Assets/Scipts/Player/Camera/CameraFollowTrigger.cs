using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollowTrigger : MonoBehaviour
{
    [Header("Player")]
    [SerializeField] GameObject player;

    [Header("Camera")]
    [SerializeField] GameObject cam;


    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject == player)
        {
            cam.transform.parent = player.transform;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject == player)
        {
            cam.transform.parent = null;
        }
    }
}
