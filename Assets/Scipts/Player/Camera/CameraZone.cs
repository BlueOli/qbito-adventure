using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraZone : MonoBehaviour
{
    public GameObject gameObjCam;
    public Camera cam;
    private Vector3 origPos, targetPos;
    private float timeToMove = 0.5f;

    private float origZoom, targetZoom;

    private float timeToRotate = 0.5f;

    public bool enPuzzle = false;
    public bool enIsla = true;

    Vector3 offsetInicioJuego = new Vector3(-26f, 19f, -19.8f);
    Vector3 offsetTutorialA = new Vector3(-25f, 20f, -25f);
    Vector3 offsetTutorialB = new Vector3(-18.5f, 20f, -25f);
    Vector3 offsetTutorialBDesc = new Vector3(-23.5f, 17f, -18f);
    Vector3 offsetTutorialC = new Vector3(-18.5f, 20f, -25f);
    Vector3 offsetIslaCentral = new Vector3(-25f, 20.4f, -25f);
    Vector3 offsetRotarNorte = new Vector3(25f, 20.4f, -35f);
    Vector3 offsetRotarSur = new Vector3(-25f, 20.4f, 35f);
    Vector3 offsetIslaNorte = new Vector3(25f, 20.4f, -25f);
    Vector3 offsetIslaSur = new Vector3(-25f, 20.4f, 25f);



    public Vector3 nuevaPos;

    public bool isMoving = false;

    Vector3 offsetIslaInicial = new Vector3(-25f, 19.4f, -25f);
    Vector3 offsetPuzzle = new Vector3(-25f, 18.5f, -25f);
    Vector3 offsetIsla = new Vector3(-25f, 21f, -25f);

    public Vector3 nuevaPosIslaInicial;
    public Vector3 nuevaPosPuzzle;
    public Vector3 nuevaPosIsla;

    private void Start()
    {
        cam = gameObjCam.transform.GetComponent<Camera>();
    }

    private IEnumerator MoveCamera(Vector3 target)
    {
        isMoving = true;

        float elapsedTime = 0;

        origPos = gameObjCam.transform.position;
        targetPos = target;

        while (elapsedTime < timeToMove)
        {
            gameObjCam.transform.position = Vector3.Lerp(origPos, targetPos, (elapsedTime / timeToMove));
            elapsedTime += Time.deltaTime;
            yield return null;
        }

        gameObjCam.transform.position = targetPos;

        isMoving = false;
    }

    private IEnumerator ZoomCamera(float target)
    {
        float elapsedTime = 0;

        origZoom = cam.orthographicSize;
        targetZoom = target;

        while (elapsedTime < timeToMove)
        {
            cam.orthographicSize = Vector2.Lerp(new Vector2(origZoom, 0), new Vector2(targetZoom, 0), (elapsedTime / timeToMove)).x;
            elapsedTime += Time.deltaTime;
            yield return null;
        }

        cam.orthographicSize = target;
    }

    private void OnTriggerEnter(Collider other)
    {
        GameObject zona = other.transform.gameObject;
        Debug.Log(zona.name);
       
        nuevaPosIslaInicial = other.gameObject.transform.position + offsetIslaInicial;
        nuevaPosPuzzle = other.gameObject.transform.position + offsetPuzzle;
        nuevaPosIsla = other.gameObject.transform.position + offsetIsla;

        if (!isMoving)
        {
            switch (zona.name)
            {

                #region Tutorial

                case "Inicio":
                    nuevaPos = zona.transform.position + offsetInicioJuego;
                    StartCoroutine(MoveCamera(nuevaPos));
                    StartCoroutine(ZoomCamera(3f));
                    StartCoroutine(RotateCamera(new Vector3(30, 45, 0), true));
                    enPuzzle = true;
                    enIsla = false;
                    break;

                case "TutorialA":
                    nuevaPos = zona.transform.position + offsetTutorialA;
                    StartCoroutine(MoveCamera(nuevaPos));
                    StartCoroutine(ZoomCamera(5f));
                    StartCoroutine(RotateCamera(new Vector3(30, 45, 0), true));
                    enPuzzle = true;
                    enIsla = false;
                    break;

                case "TutorialB":
                    nuevaPos = zona.transform.position + offsetTutorialB;
                    StartCoroutine(MoveCamera(nuevaPos));
                    StartCoroutine(ZoomCamera(5f));
                    StartCoroutine(RotateCamera(new Vector3(30, 45, 0), true));
                    enPuzzle = true;
                    enIsla = false;
                    break;

                case "TutorialBDesc":
                    nuevaPos = zona.transform.position + offsetTutorialBDesc;
                    StartCoroutine(MoveCamera(nuevaPos));
                    StartCoroutine(ZoomCamera(6f));
                    StartCoroutine(RotateCamera(new Vector3(30, 45, 0), true));
                    enPuzzle = true;
                    enIsla = false;
                    break;

                case "TutorialC":
                    nuevaPos = zona.transform.position + offsetTutorialC;
                    StartCoroutine(MoveCamera(nuevaPos));
                    StartCoroutine(ZoomCamera(5f));
                    StartCoroutine(RotateCamera(new Vector3(30, 45, 0), true));
                    enPuzzle = true;
                    enIsla = false;
                    break;

                #endregion

                case "IslaCentral":
                    nuevaPos = zona.transform.position + offsetIslaCentral;
                    StartCoroutine(MoveCamera(nuevaPos));
                    StartCoroutine(ZoomCamera(7f));
                    StartCoroutine(RotateCamera(new Vector3(30, 45, 0), false));
                    enPuzzle = false;
                    enIsla = true;
                    break;

                case "RotarNorte":
                    nuevaPos = zona.transform.position + offsetRotarNorte;
                    StartCoroutine(MoveCamera(nuevaPos));
                    StartCoroutine(ZoomCamera(7f));
                    StartCoroutine(RotateCamera(new Vector3(30, -45, 0), true));
                    enPuzzle = false;
                    enIsla = true;
                    break;

                case "RotarSur":
                    nuevaPos = zona.transform.position + offsetRotarSur;
                    StartCoroutine(MoveCamera(nuevaPos));
                    StartCoroutine(ZoomCamera(7f));
                    StartCoroutine(RotateCamera(new Vector3(30, 135, 0), true));
                    enPuzzle = false;
                    enIsla = true;
                    break;

                #region Islas Norte

                case "IslaNorte":
                    nuevaPos = zona.transform.position + offsetIslaNorte;
                    StartCoroutine(MoveCamera(nuevaPos));
                    StartCoroutine(ZoomCamera(7f));
                    StartCoroutine(RotateCamera(new Vector3(30, -45, 0), true));
                    enPuzzle = false;
                    enIsla = true;
                    break;

                #region Puzzles Norte

                case "PuzzleNorteA":
                    nuevaPos = zona.transform.position + offsetIslaNorte;
                    StartCoroutine(MoveCamera(nuevaPos));
                    StartCoroutine(ZoomCamera(7f));
                    StartCoroutine(RotateCamera(new Vector3(30, -45, 0), false));
                    enPuzzle = false;
                    enIsla = true;
                    break;

                case "PuzzleNorteB":
                    nuevaPos = zona.transform.position + offsetIslaNorte;
                    StartCoroutine(MoveCamera(nuevaPos));
                    StartCoroutine(ZoomCamera(7f));
                    StartCoroutine(RotateCamera(new Vector3(30, -45, 0), false));
                    enPuzzle = false;
                    enIsla = true;
                    break;

                case "PuzzleNorteC":
                    nuevaPos = zona.transform.position + offsetIslaNorte;
                    StartCoroutine(MoveCamera(nuevaPos));
                    StartCoroutine(ZoomCamera(7f));
                    StartCoroutine(RotateCamera(new Vector3(30, -45, 0), false));
                    enPuzzle = false;
                    enIsla = true;
                    break;

                case "PuzzleNorteD":
                    nuevaPos = zona.transform.position + offsetIslaNorte;
                    StartCoroutine(MoveCamera(nuevaPos));
                    StartCoroutine(ZoomCamera(7f));
                    StartCoroutine(RotateCamera(new Vector3(30, -45, 0), false));
                    enPuzzle = false;
                    enIsla = true;
                    break;

                case "PuzzleNorteE":
                    nuevaPos = zona.transform.position + offsetIslaNorte;
                    StartCoroutine(MoveCamera(nuevaPos));
                    StartCoroutine(ZoomCamera(7f));
                    StartCoroutine(RotateCamera(new Vector3(30, -45, 0), false));
                    enPuzzle = false;
                    enIsla = true;
                    break;

                #endregion

                #endregion

                #region Islas Este

                case "IslaEste":
                    nuevaPos = zona.transform.position + offsetIslaCentral;
                    StartCoroutine(MoveCamera(nuevaPos));
                    StartCoroutine(ZoomCamera(7f));
                    StartCoroutine(RotateCamera(new Vector3(30, 45, 0), false));
                    enPuzzle = false;
                    enIsla = true;
                    break;

                #region Puzzles Este

                case "PuzzleEsteA":
                    nuevaPos = zona.transform.position + offsetIslaCentral;
                    StartCoroutine(MoveCamera(nuevaPos));
                    StartCoroutine(ZoomCamera(7f));
                    StartCoroutine(RotateCamera(new Vector3(30, 45, 0), false));
                    enPuzzle = false;
                    enIsla = true;
                    break;

                case "PuzzleEsteB":
                    nuevaPos = zona.transform.position + offsetIslaCentral;
                    StartCoroutine(MoveCamera(nuevaPos));
                    StartCoroutine(ZoomCamera(7f));
                    StartCoroutine(RotateCamera(new Vector3(30, 45, 0), false));
                    enPuzzle = false;
                    enIsla = true;
                    break;

                case "PuzzleEsteC":
                    nuevaPos = zona.transform.position + offsetIslaCentral;
                    StartCoroutine(MoveCamera(nuevaPos));
                    StartCoroutine(ZoomCamera(7f));
                    StartCoroutine(RotateCamera(new Vector3(30, 45, 0), false));
                    enPuzzle = false;
                    enIsla = true;
                    break;

                case "PuzzleEsteD":
                    nuevaPos = zona.transform.position + offsetIslaCentral;
                    StartCoroutine(MoveCamera(nuevaPos));
                    StartCoroutine(ZoomCamera(7f));
                    StartCoroutine(RotateCamera(new Vector3(30, 45, 0), false));
                    enPuzzle = false;
                    enIsla = true;
                    break;

                case "PuzzleEsteE":
                    nuevaPos = zona.transform.position + offsetIslaCentral;
                    StartCoroutine(MoveCamera(nuevaPos));
                    StartCoroutine(ZoomCamera(7f));
                    StartCoroutine(RotateCamera(new Vector3(30, 45, 0), false));
                    enPuzzle = false;
                    enIsla = true;
                    break;

                #endregion

                #endregion

                #region Islas Sur

                case "IslaSur":
                    nuevaPos = zona.transform.position + offsetIslaSur;
                    StartCoroutine(MoveCamera(nuevaPos));
                    StartCoroutine(ZoomCamera(7f));
                    StartCoroutine(RotateCamera(new Vector3(30, 135, 0), true));
                    enPuzzle = false;
                    enIsla = true;
                    break;

                #region Puzzles Sur

                case "PuzzleSurA":
                    nuevaPos = zona.transform.position + offsetIslaSur;
                    StartCoroutine(MoveCamera(nuevaPos));
                    StartCoroutine(ZoomCamera(7f));
                    StartCoroutine(RotateCamera(new Vector3(30, 135, 0), false));
                    enPuzzle = false;
                    enIsla = true;
                    break;

                case "PuzzleSurB":
                    nuevaPos = zona.transform.position + offsetIslaSur;
                    StartCoroutine(MoveCamera(nuevaPos));
                    StartCoroutine(ZoomCamera(7f));
                    StartCoroutine(RotateCamera(new Vector3(30, 135, 0), false));
                    enPuzzle = false;
                    enIsla = true;
                    break;

                case "PuzzleSurC":
                    nuevaPos = zona.transform.position + offsetIslaSur;
                    StartCoroutine(MoveCamera(nuevaPos));
                    StartCoroutine(ZoomCamera(7.2f));
                    StartCoroutine(RotateCamera(new Vector3(30, 135, 0), false));
                    enPuzzle = false;
                    enIsla = true;
                    break;

                case "PuzzleSurD":
                    nuevaPos = zona.transform.position + offsetIslaSur;
                    StartCoroutine(MoveCamera(nuevaPos));
                    StartCoroutine(ZoomCamera(7.2f));
                    StartCoroutine(RotateCamera(new Vector3(30, 135, 0), false));
                    enPuzzle = false;
                    enIsla = true;
                    break;

                case "PuzzleSurE":
                    nuevaPos = zona.transform.position + offsetIslaSur;
                    StartCoroutine(MoveCamera(nuevaPos));
                    StartCoroutine(ZoomCamera(7.2f));
                    StartCoroutine(RotateCamera(new Vector3(30, 135, 0), false));
                    enPuzzle = false;
                    enIsla = true;
                    break;

                    #endregion

                    #endregion
            }
        }        
    }

    private IEnumerator RotateCamera(Vector3 target, bool vueltaCompleta)
    {
        float elapsedTime = 0;

        Vector3 origRot = cam.transform.eulerAngles;
        Vector3 targetRot = target;

        Debug.Log("Sin modificar:");
        Debug.Log("Original: " + origRot + " / Target: " + targetRot);

        if (!vueltaCompleta)
        {
            if ((int)origRot.x > (int)targetRot.x && origRot.x > 180)
            {
                origRot = new Vector3(origRot.x - 360, origRot.y, origRot.z);
            }

            if ((int)origRot.y > (int)targetRot.y && origRot.y > 180)
            {
                origRot = new Vector3(origRot.x, origRot.y - 360, origRot.z);
            }

            if ((int)origRot.z > (int)targetRot.z && origRot.z > 180)
            {
                origRot = new Vector3(origRot.x, origRot.y, origRot.z - 360);
            }

            Debug.Log("Despues de modificar:");
            Debug.Log("Original: " + origRot + " / Target: " + targetRot);
        }
        else
        {
            if ((int)origRot.x - 360 == (int)targetRot.x && origRot.x > 180)
            {
                origRot = new Vector3(origRot.x - 360, origRot.y, origRot.z);
                Debug.Log("Giro completo anulado:");
                Debug.Log("Original: " + origRot + " / Target: " + targetRot);
            }

            if ((int)origRot.y - 360 == (int) targetRot.y && origRot.y > 180)
            {
                origRot = new Vector3(origRot.x, origRot.y - 360, origRot.z);
                Debug.Log("Giro completo anulado:");
                Debug.Log("Original: " + origRot + " / Target: " + targetRot);
            }

            if ((int)origRot.z - 360 == (int)targetRot.z && origRot.z > 180)
            {
                origRot = new Vector3(origRot.x, origRot.y, origRot.z - 360);
                Debug.Log("Giro completo anulado:");
                Debug.Log("Original: " + origRot + " / Target: " + targetRot);
            }

        }

        while (elapsedTime < timeToRotate)
        {
            cam.transform.eulerAngles = Vector3.Lerp(origRot, targetRot, (elapsedTime / timeToRotate));

            elapsedTime += Time.deltaTime;

            yield return null;
        }

        cam.transform.eulerAngles = targetRot;
    }
}
