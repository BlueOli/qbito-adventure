using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRotation : MonoBehaviour
{
    private Vector3 origPos, targetPos;
    private Vector3 origRot, targetRot;
    private float timeToMove = 0.5f;
    private float timeToRotate = 0.5f;

    private int lastRotation = 0;
    private int newRotation = 0;

    private Vector3[] rotations = new Vector3[4];
    private Vector3[] positions = new Vector3[4];

    private bool isMoving;
    private bool isRotating;

    public GameObject player;

    private IEnumerator MoveCamera(Vector3 target)
    {
        float elapsedTime = 0;

        origPos = transform.position;
        targetPos = target;

        while (elapsedTime < timeToMove)
        {
            transform.position = Vector3.Lerp(origPos, targetPos, (elapsedTime / timeToMove));
            elapsedTime += Time.deltaTime;
            yield return null;
        }

        transform.position = targetPos;

        isMoving = false;
    }

    private IEnumerator RotateCamera(Vector3 target)
    {
        float elapsedTime = 0;

        origRot = transform.eulerAngles;
        targetRot = target;

        while (elapsedTime < timeToRotate)
        {
            transform.eulerAngles = Vector3.Lerp(origRot, targetRot, (elapsedTime / timeToRotate));
            elapsedTime += Time.deltaTime;
            yield return null;
        }

        transform.eulerAngles = targetRot;

        isRotating = false;
    }

    private void Start()
    {
        isMoving = false;
        isRotating = false;

        positions[0] = new Vector3(-25f, 23.4f, -25f);
        rotations[0] = new Vector3(30f, 45f, 0f);

        positions[1] = new Vector3(-25f, 23.4f, 25f);
        rotations[1] = new Vector3(30f, 135f, 0f);

        positions[2] = new Vector3(25f, 23.4f, 25f);
        rotations[2] = new Vector3(30f, 225f, 0f);

        positions[3] = new Vector3(25f, 23.4f, -25f);
        rotations[3] = new Vector3(30f, 315f, 0f);
    }

    void Update()
    {
        if (DialogueManager.GetInstance().dialogueIsPlaying)
        {
            return;
        }

        if (!isMoving && !isRotating)
        {
            if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                isMoving = true;
                isRotating = true;
                player.GetComponent<TileMovement>().RotarTeclas(false);
                newRotation++;
                if (newRotation == 4)
                {
                    newRotation = 3;
                    isMoving = false;
                    isRotating = false;
                    player.GetComponent<TileMovement>().RotarTeclas(true);
                }

                
            }

            if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                isMoving = true;
                isRotating = true;
                newRotation--;
                player.GetComponent<TileMovement>().RotarTeclas(true);
                if (newRotation == -1)
                {
                    newRotation = 0;
                    isMoving = false;
                    isRotating = false;
                    player.GetComponent<TileMovement>().RotarTeclas(false);
                }

                
            }
        }
        

        
    }

    private void FixedUpdate()
    {
        if (newRotation != lastRotation)
        {
            /*if (player.GetComponent<CameraZone>().enPuzzle)
            {
                StartCoroutine(MoveCamera(positions[newRotation] + player.GetComponent<CameraZone>().nuevaPosPuzzle));
            }
            else if (player.GetComponent<CameraZone>().enIsla)
            {
                StartCoroutine(MoveCamera(positions[newRotation] + player.GetComponent<CameraZone>().nuevaPosIsla));
            }*/

            StartCoroutine(MoveCamera(positions[newRotation]));
            StartCoroutine(RotateCamera(rotations[newRotation]));

            lastRotation = newRotation;
        }
        else
        {
            return;
        }
    }


}
