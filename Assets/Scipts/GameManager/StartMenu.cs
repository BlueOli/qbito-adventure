using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartMenu : MonoBehaviour
{
    public GameObject player;
    public GameObject menu;

    public void StartGame()
    {
        player.GetComponent<TileMovement>().enabled = true;
        menu.SetActive(false);
    }

    public void StopGame()
    {
        player.GetComponent<TileMovement>().enabled = false;
    }
}
