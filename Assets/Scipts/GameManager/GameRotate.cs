using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameRotate : MonoBehaviour
{
    [Header("Player")]
    [SerializeField] private GameObject player;

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private Vector3 GetFaceDirection(GameObject player)
    {
        switch (player.transform.GetChild(0).eulerAngles.y)
        {
            case 0:
                return Vector3.right;
            case 90:
                return Vector3.back;
            case 180:
                return Vector3.left;
            case 270:
                return Vector3.forward;
            default:
                return Vector3.zero;
        }
    }
}
